# -*- coding: utf-8 -*-

{
    'name': 'Customer Feedback Form',
    'summary': 'Customer Service Form that added customer feedback and store it into database',
    'version': '1.0',
    'author': 'Pragmatic Techsoft Pvt. Ltd.',
    'website': 'www.pragtech.co.in',
    'category': 'Theme',
    'description': """  Use themes to simplify the process of creating professional designer-looking presentations
    Applying a new theme changes the major details of your website.
    
    
    """,
    'sequence': 4,
    'depends': ['website'],
    'demo': [],
    'data': [
                'views/layout.xml',
                'views/assets.xml',
                'views/pages.xml',
                'views/contact.xml',
                'views/snippets.xml',
                'views/options.xml',
                'views/videos.xml',
               

    ],
    'css': ['static/src/css/base.css'],
    'auto_install': False,
    'application': True,
    'installable': True,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
